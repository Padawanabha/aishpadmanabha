Title: Aishwarya Padmanabha
Date: 2019-06-02 10:10
Slug: hello
Authors: Aishwarya Padmanabha

I am a software consultant at Adapt Ready. I help co-organize PyLadies Bengaluru's events and have a soft corner for anything involving Python. I was an active volunteer for Free Software Movement Karnataka and Camp Diaries. Apart from tech, I can hold a tune, pen my thoughts and love to draw my ideas on paper and on Inkscape. I love diversity, knowledge, networking and tech, and will grab any opportunity where I can effectively contribute to them.
